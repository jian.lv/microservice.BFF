﻿import * as express from "express";
import * as http from "http";
import * as path from "path";
import * as bodyParser from "body-parser";
import * as errorHandler from "errorhandler";
import * as cookieParser from "cookie-parser";
import * as favicon from "serve-favicon";
import * as logger from "morgan";
import * as methodOverride from "method-override";
import * as compression from "compression";
import * as fs from "fs";
import * as contextService from "request-context";

import * as router from "./utils/router";
import * as _ from "lodash";
var sse = require("sse-stream")("/sse");

var app = express();
var server = http.createServer(app);
app.head("/", (req, res, next) => {
  res.end();
});
sse.install(server);
sse.on('connection', function (client) {
  client.send('hi there!');
})
// all environments
app.set("httpPort", process.env.HTTPPORT || 80);
app.set("serviceName", process.env.SERVICENAME || "BFF");
app.use((req, res, next) => {
  res.locals.startEpoch = Date.now();
  next();
});
app.use(favicon(path.join(__dirname, "/favicon.ico")));
app.use(logger("dev"));
app.use(methodOverride());
app.use(cookieParser());
app.use(contextService.middleware("req"));

app.use(bodyParser.json({ limit: "50mb" }));
app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 1000000
  })
);

var routers = JSON.parse(fs.readFileSync(__dirname + "/router.json", "utf8"));

app.use(compression());
//across cros
app.use((req: express.Request, res: express.Response, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Content-Type, Content-Length, Authorization, Accept, X-Requested-With"
  );
  res.setHeader("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  res.setHeader("Cache-Control", "no-cache");
  res.setHeader("Pragma", "no-cache");
  res.setHeader("Expires", "-1");

  next();
});



function startServer() {
  process.on("unhandledRejection", function(reason, p) {
    console.log("Unhandled Rejection at: Promise ", p, " reason: ", reason);
  });

  // 启动纯http服务器
  server.listen(app.get("httpPort"), function() {
    console.log(
      "Http server listening on port " +
        app.get("httpPort") +
        " ---- " +
        new Date()
    );
  });
}
function registerRouters() {
 
  console.log(
    "======================begin load Manual public apiservice============================"
  );
  routers.public.forEach(router => {
    var modulefile = require(router.file);
    var apiurl = router.url;
    app.use(apiurl, modulefile);
    console.log("loadsuccess:" + apiurl);
    for (var index = 0; index < modulefile.stack.length; index++) {
      var element = modulefile.stack[index];
      if (element.route) {
        var keys = Object.keys(element.route.methods).join("");
        console.log(_.padEnd(keys, 11) + ":" + apiurl + element.route.path);
      }
    }
  });

  app.use(function(req, res, next) {
    // contextService.setContext("req:user", req.session.user);
    next();
  });
  console.log(
    "======================begin load automatic private apiservice============================"
  );
  router.createRouter(app, __dirname + "/server/private", "", "");

}

// // Graceful shutdown
process.on("SIGTERM", () => {
  server.close(err => {
    if (err) {
      console.error(err);
      process.exit(1);
    }

    process.exit(0);
  });
});
// development only
function start() {
  app.use(errorHandler());
  registerRouters();
  startServer();
}
start();