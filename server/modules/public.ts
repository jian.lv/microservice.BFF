import * as express from "express";
var router = express.Router();


router.get("/heartbeat", (req, res) => {
    res.send({ status: 'active' })
})

router.get("/whois", (req, res) => {
    res.send({ status:"ok", service:res.app.get('serviceName'), data:{ user: 'test' } })
})
router.post("/whois", (req, res) => {
    res.send({ status:"ok", service:res.app.get('serviceName'), data:{ user: 'test',other:req.body } })
})
export =router;