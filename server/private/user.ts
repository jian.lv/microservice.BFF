import * as express from "express";
import * as fetch from "../../utils/utils";
import {randomName} from '../BLL/common/randomName'
var router = express.Router();
router.get("/", async (req: express.Request, res: express.Response) => {
    let size = req.query.pageSize;
    size = size ? parseInt(size) : 10;
    size = size || 10;
    let datas = []
    var isSimple = true;
    for (let index = 0; index < size; index++) {
         
        var len = index % 2;
        len += 2;
        const element = {
            userName: randomName(isSimple,len),
            age: parseInt((Math.random()*100).toFixed(0))
        };
        
        isSimple = !isSimple;
        datas.push(element);
        
    }
    res.send({status:200,result:"ok",data:datas,length:datas.length})
})
/**
 * 调用后台服务
 * 这个例子调用的是本服务的方法
 */
router.get('/fromservice', async (req: express.Request, res: express.Response) => { 
    let bakendUrl = 'http://BFF/api/user'
    let result = await fetch.getFormPublicService(bakendUrl);
    res.send(result)
})
/**
 * 调用外部方法
 */
router.get('/news', async (req: express.Request, res: express.Response) => { 
    let bakendUrl = 'https://news.ycombinator.com/newest'
    let result = await fetch.getFormPublicService(bakendUrl);
    res.send(result)
})

export =router;