import * as _ from "lodash";
import * as moment from "moment";


/**
 * 根据user的userName获取user的employeeName
 * @param user 数据user
 * @param vendorCode 厂商编号
 * @param userList 用户列
 */
export function getUserEmployeeName(user: string, vendorCode: string, userList: any) {
    let userName = user;
    let userInfo = _.find(userList, (u: any) => { return u.userName === userName; });
    if (userInfo && userInfo.employeeName && userInfo.employeeName.length > 0) {
        return userInfo.employeeName;
    } else {
        return userName;
    }
}

/**
 * 软删除处理
 * @param obj 需要处理的obj
 */
export function getDelCreateObj(obj: any) {
    obj.del = false;
    obj.delBy = null;
    obj.delByUser = null;
    obj.delAt = null;
    return obj
}

/**
 * 格式化导出文件的名称
 * @param vendorName 厂商简称，eg：“海岸线”
 * @param fileName 导出项目的名称，eg：“工序列表”
 * @param suffix 文件类型（带.符号），eg：“.xlsx”、“.txt”
 */
export function formatOutputName(vendorName: string, fileName: string, suffix: string) {
    if (!vendorName) {
        vendorName = "";
    } else {
        vendorName += "_";
    }

    if (!fileName) {
        fileName = "数据";
    }
    if (!suffix) {
        suffix = "";
    }

    return vendorName + fileName + "_" + moment().format("YYYYMMDD") + suffix;
}

export function isGrouped(groupCodes, hqCode) {
    var target = groupCodes && _.includes(groupCodes, hqCode);
    return target;
};