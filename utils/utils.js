"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
var NumberTypes;
(function (NumberTypes) {
    NumberTypes[NumberTypes["int"] = 0] = "int";
    NumberTypes[NumberTypes["float"] = 1] = "float";
})(NumberTypes = exports.NumberTypes || (exports.NumberTypes = {}));
var Types;
(function (Types) {
    Types[Types["number"] = 0] = "number";
    Types[Types["int"] = 1] = "int";
    Types[Types["float"] = 2] = "float";
    Types[Types["string"] = 3] = "string";
    Types[Types["bool"] = 4] = "bool";
    Types[Types["date"] = 5] = "date";
    Types[Types["object"] = 6] = "object";
    Types[Types["objectId"] = 7] = "objectId";
    Types[Types["objectIdArray"] = 8] = "objectIdArray";
})(Types = exports.Types || (exports.Types = {}));
function sendToService(url, method, data, token) {
    return __awaiter(this, void 0, void 0, function* () {
        var opt = {
            url,
            method
        };
        if (data) {
            opt.data = data;
        }
        if (token) {
            opt.headers = { authorization: token };
        }
        var rs = yield axios_1.default(opt);
        return rs.data;
    });
}
exports.sendToService = sendToService;
function getFormService(req, url) {
    return __awaiter(this, void 0, void 0, function* () {
        //if need to check auth
        var token = req.header("authorization");
        return yield sendToService(url, "get", null, token);
    });
}
exports.getFormService = getFormService;
function postToService(req, url, form) {
    return __awaiter(this, void 0, void 0, function* () {
        var token = req.header("authorization");
        return yield sendToService(url, "post", form, token);
    });
}
exports.postToService = postToService;
/**
 * 发送公开接口请求
 * @param req
 * @param url
 * @param form
 */
function postToPublicService(req, url, form) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield sendToService(url, "post", form);
    });
}
exports.postToPublicService = postToPublicService;
/**
 * 发送公开put接口请求
 * @param req
 * @param url
 * @param form
 */
function putToPublicService(req, url, form) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield sendToService(url, "put", form);
    });
}
exports.putToPublicService = putToPublicService;
function getFormPublicService(url) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield sendToService(url, "get", null);
    });
}
exports.getFormPublicService = getFormPublicService;
function putToService(req, url, form) {
    return __awaiter(this, void 0, void 0, function* () {
        var token = req.header("authorization");
        return yield sendToService(url, "put", form, token);
    });
}
exports.putToService = putToService;
function deleteToPublicService(url) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield sendToService(url, "delete");
    });
}
exports.deleteToPublicService = deleteToPublicService;
function delToService(req, url, form) {
    return __awaiter(this, void 0, void 0, function* () {
        var token = req.header("authorization");
        return yield sendToService(url, "delete", form, token);
    });
}
exports.delToService = delToService;
//# sourceMappingURL=utils.js.map