﻿
import axios from "axios";

export enum NumberTypes {
    int,
    float,
}

export enum Types {
    number, // 等同int
    int,
    float,
    string,
    bool,
    date,
    object,
    objectId,
    objectIdArray
}


export async function sendToService(url: string, method: string, data?: any, token?: string): Promise<any> {
    var opt: any = {
        url,
        method
    }
    if (data) {
        opt.data = data
    }
    if (token) {
        opt.headers = { authorization: token }
    }
    var rs = await axios(opt)
    return rs.data;
}
export async function getFormService(req, url): Promise<any> {
    //if need to check auth
   var token = req.header("authorization")
    return await sendToService(url, "get", null, token)
    
}
export async function postToService(req, url, form): Promise<any> {
    var token = req.header("authorization")
    return await sendToService(url, "post", form, token)

}
/**
 * 发送公开接口请求
 * @param req
 * @param url
 * @param form
 */
export async function postToPublicService(req, url, form): Promise<any> {
    return await sendToService(url, "post", form)
   
}
/**
 * 发送公开put接口请求
 * @param req
 * @param url
 * @param form
 */
export async function putToPublicService(req, url, form): Promise<any> {
    return await sendToService(url, "put", form)
    
}
export async function getFormPublicService(url): Promise<any> {
    return await sendToService(url, "get", null)
    
}
export async function putToService(req, url, form): Promise<any> {
    var token = req.header("authorization")
    return await sendToService(url, "put", form, token)
   
}
export async function deleteToPublicService(url): Promise<any> {
    return await sendToService(url, "delete")
}
export async function delToService(req, url, form): Promise<any> {
    var token = req.header("authorization")
    return await sendToService(url, "delete", form, token)
   
}



