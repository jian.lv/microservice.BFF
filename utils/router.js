"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fs = require("fs");
const _ = require("lodash");
function createRouter(app, rootdir, cpath, file) {
    var fullpath = path.join(rootdir, cpath, file);
    var stat = fs.lstatSync(fullpath);
    if (stat.isDirectory()) {
        var exists = fs.existsSync(fullpath + ".js");
        if (exists)
            return;
        var subfiles = fs.readdirSync(fullpath);
        subfiles.forEach(function (subfile) {
            createRouter(app, rootdir, path.join(cpath, file), subfile);
        });
    }
    else {
        if (!fullpath.endsWith(".js"))
            return;
        var modulefile = require(fullpath);
        if (typeof (modulefile) !== "function" || modulefile.name !== "router")
            return;
        var apiurl;
        if (fullpath.endsWith("index.js")) {
            apiurl = '/' + app.get('serviceName') + '/api/' + cpath.replace(/\\/g, "/").replace(".js", "");
        }
        else {
            apiurl = '/' + app.get('serviceName') + '/api/' + path.join(cpath, file).replace(/\\/g, "/").replace(".js", "");
        }
        try {
            app.use(apiurl, modulefile);
            console.log("loadsuccess:" + apiurl);
            for (var index = 0; index < modulefile.stack.length; index++) {
                var element = modulefile.stack[index];
                if (element.route) {
                    var keys = Object.keys(element.route.methods).join("");
                    console.log(_.padEnd(keys, 11) + ":" + apiurl + element.route.path);
                }
            }
        }
        catch (error) {
            console.log("loaderror:" + fullpath);
        }
    }
}
exports.createRouter = createRouter;
//# sourceMappingURL=router.js.map